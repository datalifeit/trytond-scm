# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields, ModelSingleton
from trytond.pool import PoolMeta

__all__ = ['Configuration']

__metaclass__ = PoolMeta


class Configuration(ModelSingleton, ModelSQL, ModelView):
    """SCM Configuration"""
    __name__ = 'scm.configuration'

    repo_path = fields.Char('Repositories path')
    clone_on_create = fields.Boolean('Clone module on create')


    @classmethod
    def __setup__(cls):
        super(Configuration, cls).__setup__()
        cls._error_messages.update({
            'missing_repo_path':
                ('Must define a path for repositories in SCM Configuration.')})

    @classmethod
    def check_xml_record(cls, records, values):
        return True
