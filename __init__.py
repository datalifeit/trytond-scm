from trytond.pool import Pool
from .configuration import Configuration
from .scm import (
    SCMDeployment,
    SCMDeploymentModule,
    SCMRepository,
    SCMModule,
    Party,
    SCMDeploymentExport,
    SCMDeploymentExportStart,
    SCMDeploymentExportResult,
    SCMModuleImport,
    SCMModuleImportStart,
    SCMModuleImportResult)


def register():
    Pool.register(
        Configuration,
        Party,
        SCMRepository,
        SCMModule,
        SCMDeployment,
        SCMDeploymentModule,
        SCMDeploymentExportStart,
        SCMDeploymentExportResult,
        SCMModuleImportStart,
        SCMModuleImportResult,
        module='scm', type_='model'
    )
    Pool.register(
        SCMDeploymentExport,
        SCMModuleImport,
        module='scm', type_='wizard'
    )
