# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.exceptions import UserError
from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import POOL, DB_NAME, USER, CONTEXT
from trytond.transaction import Transaction


class SCMTestCase(ModuleTestCase):
    """SCM Test module"""
    module = 'scm'

    def setUp(self):
        super(SCMTestCase, self).setUp()
        self._module = POOL.get('scm.module')
        self.repo = POOL.get('scm.repository')
        self.deployment = POOL.get('scm.deployment')

    def test0010module(self):
        """Create module"""
        with Transaction().start(DB_NAME, USER,
                                 context=CONTEXT) as transaction:

            repo1, = self.repo.create([{
                'name': 'party',
                'url': 'http://hg.tryton.org/modules/party',
                'local_path': 'foundation',
            }])
            module1, = self._module.create([{
                'name': 'party',
                'repository': repo1.id,
                'local_path': 'foundation'
            }])
            self.assert_(module1.id)
            self.assert_(module1.active)
            transaction.cursor.commit()

    def test0020module_name(self):
        """Test scm module name constraint"""
        with Transaction().start(DB_NAME, USER, context=CONTEXT):
            module1, = self._module.search([], limit=1)

            name = module1.name

            name2, = self._module.create([{
                'name': '%s_2' % name,
                'repository': module1.repository.id,
                'local_path': 'foundation'
            }])

            self.assertRaises(Exception, self._module.write,
                              [name2], {
                                  'name': name,
                              })

    def test0030deployment(self):
        """Create deployment"""
        with Transaction().start(DB_NAME, USER,
                                 context=CONTEXT) as transaction:
            deployment1, = self.deployment.create([{
                'name': 'Deployment 1',
                        'version': '3.2'
            }])
            self.assert_(deployment1.id)
            self.assert_(deployment1.active)
            transaction.cursor.commit()

    def test0040deployment_module(self):
        """Add module to deployment"""
        with Transaction().start(DB_NAME, USER, context=CONTEXT):
            deployment1, = self.deployment.search([], limit=1)
            module1, = self._module.search([], limit=1)

            self.deployment.write([deployment1],
                                  {'modules': [('add', [module1.id])]})
            self.assert_(deployment1.modules)

    def test0050module_dependencies(self):
        """Module dependencies"""
        with Transaction().start(DB_NAME, USER, context=CONTEXT):
            deployment1, = self.deployment.search([], limit=1)
            module1, = self._module.search([], limit=1)

            self.assertRaises(
                UserError, module1.get_dependencies, deployment1.version)

    def test0060deployment_export(self):
        """Deployment exporting"""
        with Transaction().start(DB_NAME, USER,
                                 context=CONTEXT):

            repo1, = self.repo.create([{
                'name': 'country',
                'url': 'http://hg.tryton.org/modules/country',
                'local_path': 'foundation'
            }])
            module1, = self._module.create([{
                'name': 'country',
                'repository': repo1.id,
                'local_path': 'foundation'
            }])
            deployment1, = self.deployment.create([{
                'name': 'Deployment 2',
                        'version': '3.2',
                        'modules': [('add', [module1.id])]
            }])
            configuration = deployment1.export_configuration()
            self.assert_(configuration)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(SCMTestCase))
    return suite
